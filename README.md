## Beer Challenge (Visio)
---

### Abstract

In beer fabrication, a control quality method commonly used is to count and analyze the ratio betweenlive and dead yeast cells, done using a microscope to visually measure each amount of cells in a Neubauer chamber containing bromothimol blue diluting production samples. A computional approach is proposed to automatize cell states recognition and quantification.

### Introduction

In industrial scale fermentation processes, control quality methods are fundamental to ensure quality and have direct impact on final product. In beer brewing industries, one of those procedures involves the analysis of live and dead yeast cells ratio from fermentation samples. By diluting each sample with bromothymol blue into Neubauer chambers, where cell count can be done using a microscope, cells can be visualized and each state is recognized by its colors.   
Bromothymol blue usage permits to differ between alive and dead cells by visual constrast, as dead cells gets colored while healthy cells keep uncolored.
	
### Methods

The challenge involves automatizing this visual procedure, where a collections of images from a Neubaer chamber are given and then it's expected to compute the total number of cells, number of live and dead and then the ratio betweeen them.

#### Dataset

Dataset contains 50 images, containing parts of two samples in Neubauer chambers magnified 600x, provided by Visio Video Analytics itself, the challenge issuer. Each sample should be assembled by stitching a sequence of 25 images, indexed by (i,j) in [1,5]x[1,5], where some overlap is expected to happen.

Image (1,1) | Image (1,2) | image (1,3)
-|-|-
![image 1](data/sequencia_1/seq1_1_1.jpg) | ![image 2](data/sequencia_1/seq1_1_2.jpg) | ![image 3](data/sequencia_1/seq1_1_3.jpg)


#### Grid Reconstruction

Grid reconstruction involves the concept of image stitching, where image parts are conected to each other based on local information matching and then correctly unified. 

TODO utilize opencv stitching method. I believe using Harris corner detection is possible  to find common keypoints between images.

#### Cell identification

Using HoughCircles, from opencv, was possible to identify part of the cells in the image. To improve results, aiming to reduce the color dimensionality to improve contrast could be useful. Using K means, was possible to drastically reduce the color variety, remaining just to count each value occurance to then select with color to use to process, as binary masks could be created from zeroing other colors in image and maximizing the desired one.

#### Differentiation between dead/live cells

TODO

#### Counting 

TODO - Counting can be done with HoughCircles itself, as it have coordinates to all circles found.

### Results

TODO

### Conclusion

TODO

---

### Ricardo A. Araujo. 2021
