import numpy as np
import imageio as imgl
import matplotlib.pyplot as plt

# defines

# main

# load image set

basic_path = "data/sequencia_1/seq1"
image = []
for i in np.arange(1,6):
    line = []
    for j in np.arange(1,6):
        img = imgl.imread(basic_path + "_" + str(i) + "_" + str(j) + ".jpg")
        line.append(img)
    image.append(line)

# lets visualizate one example first
#plt.figure(figsize=(15,10))
#plt.subplot(131); plt.imshow(image[0][0][:,:,0], cmap="gray")
#plt.subplot(132); plt.imshow(image[0][0][:,:,1], cmap="gray")
#plt.subplot(133); plt.imshow(image[0][0][:,:,2], cmap="gray")
#plt.show()
