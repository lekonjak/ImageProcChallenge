# Desafio da Cerveja | Visio: Dataset

## Descrição do Dataset:

Dataset de contagem de células: Amostra de fermentação diluida em azul de metileno, vista em câmara de Neubauer a 600x

## Número de imagens: 

50 imagens, divididas em 2 sequências de 25 imagens cada

## Formato das imagens:

JPG

## Padrão de nomenclatura das imagens

```
seq{numero_da_sequencia}_{linha}_{coluna}.jpg
```
sendo:

```
{numero_da_sequencia}: Número que identifica à qual amostra aquela imagem faz parte. Pode ser 1 ou 2.
{linha}: Linha à qual a imagem pertence dentro do mosaico. Vai de 1 até 5.
{coluna}: Coluna à qual a imagem pertence dentro do mosaico. Vai de 1 até 5.
```